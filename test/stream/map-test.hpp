#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void mapPrimitiveMapperNull(){
    try{
        Stream<int>::of( { 10 } ).map<int>( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[mapPrimitiveMapperNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void mapPrimitiveMapperPrimitive(){
    auto stream = Stream<std::string>::of( { 
        std::string("10"), 
        std::string("10"), 
        std::string("5") 
    } ).map<int>( []( std::string const value ){ return std::stoi( value ); } );
    assert( stream.size() == 3 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0] == 10 );
    assert( stream.toArray().array[1] == 10 );
    assert( stream.toArray().array[2] == 5 );
    assert( stream.toArray().length == 3 );
    std::cout << "[TEST]::[mapPrimitiveMapperPrimitive]::SUCCESS" << std::endl;
}

void mapPrimitiveMapperAbstract(){
    class Test{ 
        public: 
            int x = 0;
            Test(){}
            Test( int x ) : x( x ){} 
    };
    auto stream = Stream<std::string>::of( { 
        std::string("10"), 
        std::string("10"), 
        std::string("5") 
    } ).map<Test>( []( std::string const value ){ return Test( std::stoi( value ) ); } );
    assert( stream.size() == 3 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0].x == 10 );
    assert( stream.toArray().array[1].x == 10 );
    assert( stream.toArray().array[2].x == 5 );
    assert( stream.toArray().length == 3 );
    std::cout << "[TEST]::[mapPrimitiveMapperAbstract]::SUCCESS" << std::endl;
}

void mapAbstractMapperNull(){
    class Test{ public: int x = 10; };
    try{
        Stream<Test>::of( { Test() } ).map<Test>( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[mapAbstractMapperNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void mapAbstractMapperPrimitive(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::of( { Test() } ).map<int>( []( Test const value ){ return value.x; } );
    assert( stream.size() == 1 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0] == 10 );
    assert( stream.toArray().length == 1 );
    std::cout << "[TEST]::[mapAbstractMapperPrimitive]::SUCCESS" << std::endl;
}

void mapAbstractMapperAbstract(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::of( { Test() } ).map<Test>( []( Test value ){ 
        value.x = 20;
        return value;
    } );
    assert( stream.size() == 1 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0].x == 20 );
    assert( stream.toArray().length == 1 );
    std::cout << "[TEST]::[mapAbstractMapperAbstract]::SUCCESS" << std::endl;
}

void mapPrimitiveEmpty(){
    auto stream = Stream<std::string>::empty().map<int>( []( std::string const value ){ return std::stoi( value ); } );
    assert( stream.size() == 0 );
    assert( stream.isEmpty() );
    assert( stream.toArray().length == 0 );
    std::cout << "[TEST]::[mapPrimitiveEmpty]::SUCCESS" << std::endl;
}

void mapAbstractEmpty(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::empty().map<int>( []( Test const value ){ return value.x; } );
    assert( stream.size() == 0 );
    assert( stream.isEmpty() );
    assert( stream.toArray().length == 0 );
    std::cout << "[TEST]::[mapAbstractEmpty]::SUCCESS" << std::endl;
}