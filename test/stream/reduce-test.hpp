#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void reducePrimitiveBinaryOperationsNull(){
    try{
        Stream<int>::of( { 10 } ).reduce( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[reducePrimitiveBinaryOperationsNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void reducePrimitiveBinaryOperationsEmpty(){
    auto stream = Stream<int>::empty().reduce( []( int const value, int accumulator ){ return accumulator += value; } );
    assert( stream.get() == 0 );
    std::cout << "[TEST]::[reducePrimitiveBinaryOperationsEmpty]::SUCCESS" << std::endl;
}

void reducePrimitiveBinaryOperations(){
    auto stream = Stream<int>::of( { 10, 10, 10, 10 } ).reduce( []( int const value, int accumulator ){ return accumulator += value; } );
    assert( stream.get() == 40 );
    std::cout << "[TEST]::[reducePrimitiveBinaryOperations]::SUCCESS" << std::endl;
}

void reduceAbstractBinaryOperationsNull(){
    class Test{ public: int x = 10; };
    try{
        Stream<Test>::of( { Test() } ).reduce( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[reduceAbstractBinaryOperationsNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void reduceAbstractBinaryOperationsEmpty(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::empty().reduce( []( Test const value, Test accumulator ){ 
        accumulator.x += value.x;
        return accumulator;
    } );
    assert( stream.get().x == 10 );
    std::cout << "[TEST]::[reduceAbstractBinaryOperationsEmpty]::SUCCESS" << std::endl;
}

void reduceAbstractBinaryOperations(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::of( { Test(), Test() } ).reduce( []( Test const value, Test accumulator ){ 
        accumulator.x += value.x;
        return accumulator;
    } );
    assert( stream.get().x == 30 );
    std::cout << "[TEST]::[reduceAbstractBinaryOperations]::SUCCESS" << std::endl;
}