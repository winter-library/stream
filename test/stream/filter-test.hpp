#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void filterPrimitivePredicateNull(){
    try{
        Stream<int>::of( { 10 } ).filter( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[filterPrimitivePredicateNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void filterPrimitivePredicate(){
    auto stream = Stream<int>::of( { 10, 10, 10, 10, 5 } ).filter( []( int const value ){ return value == 5; } );
    assert( stream.size() == 1 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0] == 5 );
    assert( stream.toArray().length == 1 );
    std::cout << "[TEST]::[filterPrimitivePredicate]::SUCCESS" << std::endl;
}

void filterAbstractPredicateNull(){
    class Test{ public: int x = 10; };
    try{
        Stream<Test>::of( { Test() } ).filter( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[filterAbstractPredicateNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void filterAbstractPredicate(){
    class Test{ public: int x = 10; };
    Test test = Test();
    test.x = 5;
    auto stream = Stream<Test>::of( { Test(), test } ).filter( []( Test const value ){ return value.x == 5; } );
    assert( stream.size() == 1 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0].x == 5 );
    assert( stream.toArray().length == 1 );
    std::cout << "[TEST]::[filterAbstractPredicate]::SUCCESS" << std::endl;
}

void filterPrimitiveEmpty(){
    auto stream = Stream<int>::empty().filter( []( int const value ){ return value == 5; } );
    assert( stream.size() == 0 );
    assert( stream.isEmpty() );
    std::cout << "[TEST]::[filterPrimitiveEmpty]::SUCCESS" << std::endl;
}

void filterAbstractPredicateEmpty(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::empty().filter( []( Test const value ){ return value.x == 5; } );
    assert( stream.size() == 0 );
    assert( stream.isEmpty() );
    std::cout << "[TEST]::[filterAbstractPredicateEmpty]::SUCCESS" << std::endl;
}