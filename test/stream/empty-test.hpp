#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void emptyPrimitive(){
    assert( ( Stream<int>::empty().size() == 0 ) );
    assert( ( Stream<float>::empty().size() == 0 ) );
    assert( ( Stream<char>::empty().size() == 0 ) );
    assert( ( Stream<double>::empty().size() == 0 ) );
    assert( ( Stream<std::string>::empty().size() == 0 ) );
    assert( ( Stream<bool>::empty().size() == 0 ) );
    std::cout << "[TEST]::[emptyPrimitive]::SUCCESS" << std::endl;
}

void emptyAbstract(){
    class Test{ public: int const x = 10; };
    assert( ( Stream<Test>::empty().size() == 0) );
    std::cout << "[TEST]::[emptyAbstract]::SUCCESS" << std::endl;
}