#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void anyMatchPrimitivePredicateNull(){
    try{
        Stream<int>::of( { 10 } ).anyMatch( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[anyMatchPrimitivePredicateNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void anyMatchPrimitivePredicateTrue(){
    assert( ( Stream<int>::of( { 10, 20, 20, 20, 20 } ).anyMatch( []( int const value ){ return value == 10; } ) ) );
    std::cout << "[TEST]::[anyMatchPrimitivePredicateTrue]::SUCCESS" << std::endl;
}

void anyMatchPrimitivePredicateFalse(){
    assert( ( !Stream<int>::of( { 20, 20, 20, 20, 20 } ).anyMatch( []( int const value ){ return value == 10; } ) ) );
    std::cout << "[TEST]::[anyMatchPrimitivePredicateFalse]::SUCCESS" << std::endl;
}

void anyMatchAbstractPredicateNull(){
    class Test{ public: int const x = 10; };
    try{
        Stream<Test>::of( { Test() } ).anyMatch( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[anyMatchAbstractPredicateNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void anyMatchAbstractPredicateTrue(){
    class Test{ public: int x = 10; };
    Test test = Test();
    test.x = 20;
    assert( ( Stream<Test>::of( { Test(), test } ).anyMatch( []( Test const value ){ return value.x == 10; } ) ) );
    std::cout << "[TEST]::[anyMatchAbstractPredicateTrue]::SUCCESS" << std::endl;
}

void anyMatchAbstractPredicateFalse(){
    class Test{ public: int const x = 20; };
    assert( ( !Stream<Test>::of( { Test(), Test() } ).anyMatch( []( Test const value ){ return value.x == 10; } ) ) );
    std::cout << "[TEST]::[anyMatchAbstractPredicateFalse]::SUCCESS" << std::endl;
}

void anyMatchPrimitiveEmpty(){
    assert( ( !Stream<int>::empty().anyMatch( []( int const value ){ return value == 10; } ) ) );
    std::cout << "[TEST]::[anyMatchPrimitiveEmpty]::SUCCESS" << std::endl;
}

void anyMatchAbstractEmpty(){
    class Test{ public: int x = 10; };
    assert( ( !Stream<Test>::empty().anyMatch( []( Test const value ){ return value.x == 10; } ) ) );
    std::cout << "[TEST]::[anyMatchAbstractEmpty]::SUCCESS" << std::endl;
}