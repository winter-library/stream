#pragma once

#include <iostream>
#include <exception>

class nullptr_value : public std::exception {

	public:

		explicit nullptr_value( char const * const message ) : message( message ){}

		const char * what() const throw (){ return message; }

	private:

		char const * const message;

};
